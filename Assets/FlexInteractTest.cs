﻿using System.Collections;
using System.Collections.Generic;
using NVIDIA.Flex;
using UnityEngine;

public class FlexInteractTest : MonoBehaviour {

	[SerializeField]
	FlexSourceActor waterSpawner;
	[SerializeField]
	Transform waterAttractor;

	public void Update () {
		if (Input.GetKeyDown ("space")) {
			waterSpawner.KillAllParticles ();
		}
		if (Input.GetKey ("return")) {
			waterSpawner.AttractWater (waterAttractor.position);
		}
	}
}