﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bending : MonoBehaviour {
	[HideInInspector]
	public HandInput handInput;
	public delegate void BendingAction (HandInput.HandEventArgs args);
	public class BendingEvent : UnityEvent<HandInput.HandEventArgs> { }

	public class BendingPhase {
		public HandInput.HandEvent runCondition;
		public HandInput.HandEvent[] failConditions;
		public BendingAction inBetweenAction;
		public Collider runConditionTarget;

		#region BendingPhaseConstructors
		public BendingPhase (HandInput.HandEvent runCondition) {
			this.runCondition = runCondition;
		}
		public BendingPhase (HandInput.HandEvent runCondition, HandInput.HandEvent[] failConditions) {
			this.runCondition = runCondition;
			this.failConditions = failConditions;
		}
		public BendingPhase (HandInput.HandEvent runCondition, HandInput.HandEvent[] failConditions, BendingAction inBetweenAction) {
			this.runCondition = runCondition;
			this.failConditions = failConditions;
			this.inBetweenAction = inBetweenAction;
		}
		public BendingPhase (HandInput.HandEvent runCondition, Collider runConditionTarget, HandInput.HandEvent[] failConditions, BendingAction inBetweenAction) {
			this.runCondition = runCondition;
			this.runConditionTarget = runConditionTarget;
			this.failConditions = failConditions;
			this.inBetweenAction = inBetweenAction;
		}
		#endregion
	}

	public enum BendingSpellHandType {
		SingleHanded,
		TwoHanded,
		EitherOr
	}

	public class BendingSpell {
		public string name = "";
		public List<BendingPhase> phases = new List<BendingPhase> ();
		public BendingAction bendingAction;
		public BendingSpellHandType handType;
		public HandInput.HandIndex handIndex;
		public BendingEvent bendingFailed = new BendingEvent ();
		int index;

		public BendingSpell (BendingSpellHandType handType) {
			this.handType = handType;
		}

		public void Initialize () {
			index = 0;
			phases[0].runCondition.AddListener (NextCondition);
			if (phases[0].failConditions != null) {
				foreach (HandInput.HandEvent failCondition in phases[0].failConditions) {
					failCondition.AddListener (Fail);
				}
			}
		}

		public void NextCondition (HandInput.HandEventArgs args) {
			//Remove previous listener
			phases[index].runCondition.RemoveListener (NextCondition);
			if (phases[index].failConditions != null) {
				foreach (HandInput.HandEvent failCondition in phases[index].failConditions) {
					failCondition.RemoveListener (Fail);
				}
			}

			if (phases[index].inBetweenAction != null)
				phases[index].inBetweenAction (args);

			index++;
			Debug.Log ("Next condition for " + name + " i: " + index);

			//If all runconditions are met
			if (index >= phases.Count) {
				bendingAction (args);
				return;
			}
			//Add new ones
			phases[index].runCondition.AddListener (NextCondition);
			if (phases[index].failConditions != null) {
				foreach (HandInput.HandEvent failCondition in phases[index].failConditions) {
					failCondition.AddListener (Fail);
				}
			}
		}

		public void Fail (HandInput.HandEventArgs args) {
			Debug.Log ("Failed at index: " + index);
			phases[index].runCondition.RemoveListener (NextCondition);
			if (phases[index].failConditions != null) {
				foreach (HandInput.HandEvent failCondition in phases[index].failConditions) {
					Debug.Log ("Removing fail listener");
					failCondition.RemoveListener (Fail);
				}
			}
			this.bendingFailed.Invoke (args);
		}
	}

	public virtual void Start () {
		handInput = gameObject.GetComponent<HandInput> ();
	}

}