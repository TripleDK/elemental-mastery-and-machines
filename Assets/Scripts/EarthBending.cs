﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class EarthBending : Bending {

	public float earthPower = 100f;
	public ParticleSystem targetingArea;
	public float targetingSpeed = 3f;
	public Transform earthFocus;
	public Rigidbody smallRock;
	public GameObject rockPillar;

	bool searchingEarth = false;

	GameObject heldRock = null;


	void SearchEarth (HandInput.HandEventArgs args) {
		Debug.Log ("Bout to search for Earth!");
		IEnumerator searchEarthCoroutine = CoSearchEarth (args);
		StartCoroutine (searchEarthCoroutine);
		handInput.OnHandMove.AddListener (StopSearchEarth);
	}

	void StopSearchEarth (HandInput.HandEventArgs args) {
		this.searchingEarth = false;
		targetingArea.Stop ();
	}

	private IEnumerator CoSearchEarth (HandInput.HandEventArgs args) {
		this.searchingEarth = true;
		targetingArea.transform.position = new Vector3 (handInput.transform.position.x, handInput.transform.position.y, handInput.transform.position.z + 2);
		targetingArea.Play ();
		//Set search area in front of player
		//Get middle point between hands / or just one hand
		while (this.searchingEarth) {
			Vector3 handMovement = args.handIndex == HandInput.HandIndex.LeftHand ? handInput.leftHandVelHistory[handInput.leftHandVelHistory.Count - 1] : handInput.rightHandVelHistory[handInput.rightHandVelHistory.Count - 1];
			targetingArea.transform.Translate (handMovement.x * 3, 0, handMovement.z * 3, Space.World);
			yield return new WaitForEndOfFrame ();
		}
	}

	public BendingSpell TargetEarth = new BendingSpell (Bending.BendingSpellHandType.EitherOr);
	public BendingSpell PunchRocks = new BendingSpell (Bending.BendingSpellHandType.EitherOr);
	public BendingSpell GrowWall = new BendingSpell (Bending.BendingSpellHandType.EitherOr);
	// Use this for initialization
	public override void Start () {
		base.Start();
		targetingArea.Stop ();

		TargetEarth.phases.Add (new BendingPhase (handInput.OnHandPalm, null, SearchEarth));
		TargetEarth.phases.Add (new BendingPhase (handInput.OnHandMove, null, new BendingAction (delegate { })));
		TargetEarth.bendingAction = TargetedEarth;
		TargetEarth.Initialize ();
		TargetEarth.name = "Target earth";

		PunchRocks.phases.Add (new BendingPhase (handInput.OnHandPeakSpeedPalmUp, null, SpawnRock));
		PunchRocks.phases.Add (new BendingPhase (handInput.OnHandStop, null, new BendingAction (delegate { })));
		PunchRocks.phases.Add (new BendingPhase (handInput.OnHandPeakSpeed, null, new BendingAction (delegate { })));
		PunchRocks.bendingAction = PunchRocksAction;
		PunchRocks.name = "Punch rocks";

		GrowWall.phases.Add (new BendingPhase (handInput.OnHandPeakSpeedPalmDown, null, new BendingAction (delegate { })));
		GrowWall.bendingAction = SpawnWall;
		GrowWall.name = "Raise wall";

	}

	void TargetedEarth (HandInput.HandEventArgs args) {
		PunchRocks.Initialize ();
		GrowWall.Initialize ();
	}

	void PunchRocksAction (HandInput.HandEventArgs args) {
		StopCoroutine ("CoHoldRockInAir");
		Vector3 punchPower = handInput.leftHandVelHistory[handInput.leftHandVelHistory.Count - 1];
		Debug.Log ("Punched a rock! Punch strength: " + punchPower);
		heldRock.GetComponent<Rigidbody> ().AddForce (earthPower * punchPower, ForceMode.Impulse);
		TargetEarth.Initialize ();
	}

	void SpawnRock (HandInput.HandEventArgs args) {
		Debug.Log ("Stone appears!");
		heldRock = GameObject.Instantiate (smallRock.gameObject, targetingArea.transform.position - Vector3.up * smallRock.transform.lossyScale.y, Quaternion.identity);
		StartCoroutine (CoIgnoreTerrainCollision (false));
		Vector3 spawnSpeed = handInput.leftHandVelHistory[handInput.leftHandVelHistory.Count - 1];
		heldRock.GetComponent<Rigidbody> ().AddForce (spawnSpeed * earthPower, ForceMode.Impulse);
		earthFocus.position = targetingArea.transform.position + spawnSpeed * earthPower;
		earthFocus.position = new Vector3 (earthFocus.position.x, handInput.transform.position.y + 3, earthFocus.position.z);
		GrowWall.Fail (args);
		StartCoroutine ("CoHoldRockInAir");

	}

	private IEnumerator CoHoldRockInAir () {
		Rigidbody rockRigidBody = heldRock.GetComponent<Rigidbody> ();
		while (true) {
			Vector3 focusDir = earthFocus.position - heldRock.transform.position;
			rockRigidBody.AddForce (focusDir * 10, ForceMode.Force);
			rockRigidBody.AddForce (-rockRigidBody.velocity * 5, ForceMode.Force);
			yield return new WaitForFixedUpdate ();
		}
	}

	private IEnumerator CoIgnoreTerrainCollision (bool forever = true) {
		Collider rockCollider = heldRock.GetComponent<Collider> ();
		Collider[] spawnColliders = Physics.OverlapBox (targetingArea.transform.position, rockPillar.transform.lossyScale, Quaternion.identity);
		List<Collider> terrainColliders = (Physics.OverlapBox (targetingArea.transform.position, rockPillar.transform.lossyScale, Quaternion.identity)).ToList ()
			.Where (col => col.gameObject.tag == "Terrain").ToList ();

		foreach (Collider col in terrainColliders) {
			Physics.IgnoreCollision (col, rockCollider, true);
		}

		yield return new WaitForSeconds (1);

		if (!forever) {
			foreach (Collider col in terrainColliders) {
				Physics.IgnoreCollision (col, rockCollider, false);
			}
		}
	}

	void SpawnWall (HandInput.HandEventArgs args) {
		Debug.Log ("Wall appears!");
		heldRock = GameObject.Instantiate (rockPillar, targetingArea.transform.position - Vector3.up * 2f, Quaternion.identity);
		StartCoroutine (CoIgnoreTerrainCollision (true));
		Vector3 spawnSpeed = args.velocity;
		Rigidbody wallRigid = heldRock.GetComponent<Rigidbody> ();
		wallRigid.AddForce (spawnSpeed * earthPower, ForceMode.Impulse);
		TargetEarth.Initialize ();
	}

	void OnDrawGizmos () {
		Gizmos.color = Color.blue;
		Gizmos.DrawSphere (earthFocus.transform.position, .2f);
	}
}