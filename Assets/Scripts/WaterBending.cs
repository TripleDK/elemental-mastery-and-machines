﻿using System.Collections;
using System.Collections.Generic;
using NVIDIA.Flex;
using UnityEngine;

public class WaterBending : Bending {
	public Transform waterFocus;
	public float bendingRange = 10f;
	public Transform bendingFocus;
	public FlexSourceActor waterSource;

	bool gatheringWater = false;
	IEnumerator GatherWaterCoroutine;

	BendingSpell waterThrow = new BendingSpell (Bending.BendingSpellHandType.EitherOr);

	public override void Start () {
		base.Start ();
		waterThrow.phases.Add (new BendingPhase (handInput.OnHandWater, null, GatherWater));
		waterThrow.phases.Add (new BendingPhase (handInput.OnHandPeakSpeed, new HandInput.HandEvent[] { handInput.OnHandOpen, handInput.OnHandFist }, null));
		waterThrow.bendingAction = StopGatherWater;
		waterThrow.name = "Throw water";

		waterThrow.Initialize ();
	}

	void GatherWater (HandInput.HandEventArgs args) {
		StopCoroutine (GatherWaterCoroutine);
		GatherWaterCoroutine = CoGatherWater (args);
		StartCoroutine (GatherWaterCoroutine);
	}

	IEnumerator CoGatherWater (HandInput.HandEventArgs args) {
		Debug.Log ("Gather me some water");
		gatheringWater = true;
		waterSource.isActive = true;
		waterThrow.bendingFailed.AddListener (StopGatherWater);
		while (gatheringWater) {
			Vector3 handMovement = args.handIndex == HandInput.HandIndex.LeftHand ? handInput.leftHandVelHistory[handInput.leftHandVelHistory.Count - 1] : handInput.rightHandVelHistory[handInput.rightHandVelHistory.Count - 1];
			bendingFocus.transform.Translate (handMovement * 3f);
			waterSource.startSpeed = handMovement.magnitude * 100f;
			waterSource.AttractWater (bendingFocus.position);
			yield return new WaitForEndOfFrame ();
		}
		waterThrow.bendingFailed.RemoveListener (StopGatherWater);
		waterSource.isActive = false;
		yield return new WaitForSeconds (5f);
		waterSource.KillAllParticles ();

	}

	void StopGatherWater (HandInput.HandEventArgs args) {
		Debug.Log ("Cease watering!");
		gatheringWater = false;
		waterThrow.Initialize ();
	}
}